/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pin.newox;

/**
 *
 * @author USER
 */
import java.util.Scanner;

public class Ox {

    static int round = 1;
    static char winner = '-';
    static boolean isFinish = false;
    static int row, col;
    static Scanner kb = new Scanner(System.in);
    static char table[][] = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'},};
    static char player = 'X';

    static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    static void showTable() {
        System.out.println(" 123");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println();
        }
    }

    static void showTurn() {
        System.out.println(player + " " + "turn");
    }

    static void input() {
        while (true) {
            System.out.println("Please input Row Col:");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if (table[row][col] == '-') {
                table[row][col] = player;
                break;
            }
            System.out.println("Error: table at row and col is not empty!!!");
        }
    }

    static void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkP1() {
        for (int i = 0; i < 3; i++) {
            if (table[i][i] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkP2() {
        int colx = 2;
        for (int row = 0; row < 3; row++) {
            if (table[row][colx] != player) {
                return;
            }
            colx--;
        }
        isFinish = true;
        winner = player;
    }

    static void checkDraw() {
        if (round == 10) {
            isFinish = true;
        }
    }

    static void checkWin() {
        checkRow();
        checkCol();
        checkP1();
        checkP2();
    }

    static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
        round++;
    }

    static void showResult() {
        if (winner == '-') {
            showTable();
            System.out.println("Draw!!");
        } else {
            showTable();
            System.out.println(winner + " win!!!");
        }
    }

    static void showBye() {
        System.out.println("Bye bye ....");
    }

    public static void main(String[] args) {
        showWelcome();
        do {
            showTable();
            showTurn();
            input();
            checkWin();
            switchPlayer();
            checkDraw();
        } while (!isFinish);
        showResult();
        showBye();
    }
}
